import json
import pathlib
import sys
from ast import literal_eval
from typing import List, Any

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.agent.mail import Mail


class BaseCommunicatingAgent:
    """
    Agent class that can communicate
    """

    def __init__(self) -> None:
        self.mailbox: List[Mail] = []
        self.subscribe("agent/" + str(self.id) + "/mail", self.receive_mail)

    def receive_mail(self, client, userdata, message) -> None:
        """
        Called when the agent receive a new message
        """
        raw = literal_eval(message.payload.decode("utf-8"))
        self.mailbox.append(Mail(raw.get("id"), raw.get("cycle"), raw.get("payload")))

    def send_mail(self, agent_id: int, payload: Any) -> None:
        """
        Send a mail to agent id
        :param agent_id: id of the receiver
        :param payload: anything, must be serializable with json.dumps().
        """

        new_mail = {"id": self.id, "cycle": self.nbr_cycle, "payload": payload}
        self.client.publish("agent/" + str(agent_id) + "/mail", json.dumps(new_mail))

    def next_mail(self) -> Mail:
        """
        Convenient method to use to go through the mailbox
        :return: the next mail, if no mail are in the mailbox, return None. Remove the mail from the mailbox
        """
        if len(self.mailbox) == 0:
            return None
        return self.mailbox.pop(0)

    def put_back(self, mail: Mail) -> None:
        """
        Put the mail at the end of the mailbox
        """
        self.mailbox.append(mail)

    def put_front(self, mail: Mail) -> None:
        """
        put the mail in the front of the mailbox
        """
        self.mailbox = [mail] + self.mailbox
