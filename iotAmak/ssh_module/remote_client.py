"""
remote client class file
"""


class RemoteClient:
    """
    Class used to store information about the raspberry
    """

    def __init__(self, hostname: str, user: str, password: str):
        self.hostname: str = hostname
        self.user: str = user
        self.password: str = password

    def to_send(self) -> dict:
        """
        convert the current instance in a dict, use to send it through command line
        """
        return {"hostname": self.hostname, "user": self.user, "password": self.password}
