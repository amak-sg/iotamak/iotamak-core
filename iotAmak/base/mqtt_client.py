"""
MQTT client class file
"""
from typing import Callable

from paho.mqtt.client import Client


class MqttClient:
    """
    Base class to any instance that need to interact with the broker
    """

    def __init__(self, broker_ip: str, client_id: str, broker_username: str, broker_password: str):
        self.client: Client = Client(client_id=client_id)
        self.client.username_pw_set(username=broker_username, password=broker_password)
        self.client.connect(host=broker_ip)
        self.client.loop_start()

    def subscribe(self, topic: str, fun: Callable) -> None:
        """
        subscribe to the topic, and use fun whenever you receive a message
        """
        self.client.subscribe(topic)
        self.client.message_callback_add(topic, fun)

    def publish(self, topic: str, message) -> None:
        """
        publish the message in a specified topic
        """
        self.client.publish(topic, message)
