"""
Amas class
"""
import json
from ast import literal_eval

import sys
import pathlib

sys.path.insert(0, str(pathlib.Path(__file__).parent.parent))
from iotAmak.ssh_module.remote_client import RemoteClient
from iotAmak.base.schedulable import Schedulable
from iotAmak.amas.base_amas import BaseAmas


class Amas(Schedulable, BaseAmas):
    """
    Amas class
    """

    def __init__(self, arguments: str) -> None:

        arguments = json.loads(arguments)

        broker_ip: str = arguments.get("broker_ip")
        clients: str = arguments.get("clients")
        seed: int = int(arguments.get("seed"))
        broker_username: str = str(arguments.get("broker_username"))
        broker_password: str = str(arguments.get("broker_password"))
        iot_path: str = str(arguments.get("iot_path"))
        true_client = [RemoteClient(i.get("hostname"), i.get("user"), i.get("password")) for i in literal_eval(clients)]
        experiment_folder: str = str(arguments.get("experiment_folder"))

        Schedulable.__init__(self, broker_ip, "Amas", broker_username, broker_password)
        self.subscribe("scheduler/schedulable/wakeup", self.wake_up)
        BaseAmas.__init__(self,
                          broker_ip,
                          broker_username,
                          broker_password,
                          seed,
                          iot_path,
                          true_client,
                          experiment_folder)

        self.client.publish("amas/action_done", "")

    def on_cycle_begin(self) -> None:
        """
        This method will be executed at the start of each cycle
        """
        pass

    def on_cycle_end(self) -> None:
        """
        This method will be executed at the end of each cycle
        """
        pass

    def run(self) -> None:
        """
        Main function of the amas class
        """
        self.push_agent()

        while not self.exit_bool:

            self.wait()
            if self.exit_bool:
                return

            self.publish("amas/all_metric", str(self.agents_metric))
            graph = self.to_graph()
            if graph is not None:
                self.publish("amas/graph", str(graph))
            self.on_cycle_begin()
            self.client.publish("amas/action_done", "")

            # agent cycle

            self.wait()
            self.on_cycle_end()
            self.client.publish("amas/action_done", "")

            self.nbr_cycle += 1
